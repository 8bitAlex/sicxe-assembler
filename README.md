#SDSU CS530 - Systems Programming Project

##Code Name: Iron Throne
##Official Contributors: AS, KB, JB

##Description: 	
An assembler capable of reading SCI/XE assembly code, based on Dr. Leland Becks book "System Software 3rd".

##Development Process:
	
- Phase 1: File_Parser.cc
	Read an SCI/SCIXE assembly language source code file and 
	separate all content into separate, identifiable tokens.
	
- Phase 2: Opcodetab.cc
	Lookup table for SIC/XE opcode with accompanying machine code
	and size in bytes.
	
- Phase 3: Symtab.cc, SICXE_ASM.cpp
	Holds user defined symbols.
	Pass one, load user defined symbols and set addresses.
	
- Phase 4: (Version 1.0) SICXE_ASM.cpp
	Pass two, generate machine code in .lis file.
	NOTE: Does not produce object code. Only produces .lis file.

------------------------------------------------------
##How to:
	
Download source code.
	
Navigate terminal to source code.
	
Compile the program

		Make
	
Execute program

		sicxe_asm <.asm file>

------------------------------------------------------
##Update
	
-	02FEB2014 - Version 0.1 - Created file parser
	
-	19MAR2014 - Version 0.2 - Created opcode table
	
-	28APR2014 - Version 1.0 - Official release of product version